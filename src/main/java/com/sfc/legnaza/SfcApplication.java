package com.sfc.legnaza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SfcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SfcApplication.class, args);
	}

}
