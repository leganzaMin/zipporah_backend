FROM java:8

LABEL maintainer="lsmin01@gmail.com"

VOLUME /tmp

EXPOSE 7071

ARG JAR_FILE=target/zipporah_backend-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} zipporah_backend-springboot.jar

ENTRYPOINT ["java","-jar","/zipporah_backend-springboot.jar"]


